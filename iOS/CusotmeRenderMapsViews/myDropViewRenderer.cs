﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using CoreFoundation;
using ClusteringMapXamarinForms.CustomFormElements;
using ClusteringMapXamarinForms.iOS;
using ClusteringMapXamarinForms;
using UIKit;

[assembly: ExportRenderer(typeof(MyViewDrop), typeof(myDropViewRenderer))]
namespace ClusteringMapXamarinForms.iOS
{
	public class myDropViewRenderer : ViewRenderer<MyViewDrop,UIView>
	{

		protected override void OnElementChanged(ElementChangedEventArgs<MyViewDrop> e)
		{
			base.OnElementChanged(e);
			UIAlertView alert = new UIAlertView()
			{
				Title = "alert title",
				Message = "nil",
			};
			alert.AddButton("OK");
			alert.Show();}

	
		}
	}

